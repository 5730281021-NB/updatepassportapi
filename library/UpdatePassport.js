const config = require('./config');
//const sql =  require('mssql/msnodesqlv8');
module.exports.Updatedatabase = (async function (MBCODE,CUST_ID_OLD,CUST_ID_NEW) {

    const pool = await new require('node-jt400').pool(config);
    var update_MVM01P = "update MBRFLIB/MVM01P ";
    var update_MCRTA7P = "update MBRFLIB/MCRTA7P ";
    var update_MPOTF1P = "update MBRFLIB/MPOTF1P ";
        strset = " set MBID=?";
        condition = " where MBCODE='" + MBCODE + "' and MBID='" + CUST_ID_OLD + "'";
        update_MVM01P = update_MVM01P + strset + condition;
        update_MCRTA7P = update_MCRTA7P + strset + condition;
        update_MPOTF1P = update_MPOTF1P + strset + condition;
        var update_params = [CUST_ID_NEW];

    await pool.update(update_MVM01P,update_params);
    await pool.update(update_MCRTA7P,update_params);
    await pool.update(update_MPOTF1P,update_params);

})
module.exports.queryCountry = (async function (CUST_COUNTRYCODE_NEW) {

    const pool = await new require('node-jt400').pool(config);
    st = await ("select CNTRYCD3 from MBRFLIB/CM100MP where CNTRYCD2 = '"+ CUST_COUNTRYCODE_NEW + "'");
    let q3 = await pool.query(st);
    return q3[0].CNTRYCD3;

})

module.exports.queryCard = (async function (MBCODE) {

    const pool = await new require('node-jt400').pool(config);
    st = await ("select MBMEMC,MBEXP from MBRFLIB/CM100MP where MBCODE = '"+ MBCODE + "'");
    let q3 = await pool.query(st);
    return q3;

})