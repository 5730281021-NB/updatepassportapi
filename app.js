const express = require('express')

const app = express();

const UpdatePassport = require('./library/UpdatePassport');

const bodyParser = require('body-parser');

app.listen(8126, function () {
	console.log('app listening on port 8126!');
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

app.post('/updatepassportmcard', async function (req, res) {


	//validate JSON format
	if (!req.is('application/json')) {
		st = JSON.parse({
			'RESP_CDE': '402',
			"RESP_MSG": 'Invalid Parameter "JSON Format"'
		});

		res.status(200).send(st);
		return next(
			new errors.InvalidContentError("Expects 'application/json"),
		);
	};


	var today = new Date();
	MCARD_NUM = req.body.MCARD_NUM;
	CUST_ID_OLD = req.body.CUST_ID_OLD;
	CUST_ID_NEW = req.body.CUST_ID_NEW;
	CUST_COUNTRYCODE_NEW = req.body.CUST_COUNTRYCODE_NEW;

	if(typeof MCARD_NUM == 'undefined' || typeof CUST_ID_OLD == 'undefined'  || typeof CUST_ID_NEW =='undefined'  || typeof CUST_COUNTRYCODE_NEW == 'undefined' ){
		res.status(200).send({'RESP_CDE': '401',
		'RESP_MSG': 'Missing Parameter'})
	}

	else if(!isInteger(MCARD_NUM) || MCARD_NUM.length != 16){
		res.status(200).send({'RESP_CDE': '402',
		'RESP_MSG': 'Invalid Parameter "MCARD_NUM"'})
	}
	else if (!isInteger(CUST_ID_OLD) && CUST_ID_OLD.length == 13) {
		res.status(200).send({'RESP_CDE': '402',
		'RESP_MSG': 'Invalid Parameter "CUST_ID_OLD"'})
	}
	else if (!isInteger(CUST_ID_NEW) && CUST_ID_NEW.length == 13) {
		res.status(200).send({'RESP_CDE': '402',
		'RESP_MSG': 'Invalid Parameter "CUST_ID_NEW"'})
	}
	else if (!isInteger(CUST_COUNTRYCODE_NEW) && CUST_COUNTRYCODE_NEW.length != 2) {
		res.status(200).send({'RESP_CDE': '402',
		'RESP_MSG': 'Invalid Parameter "CUST_COUTRYCODE_NEW"'})
	}
	var Country3 = await UpdatePassport.queryCountry(CUST_COUNTRYCODE_NEW);
	var newCustID = Country3 + CUST_ID_NEW;
	UpdatePassport.Updatedatabase(MCARD_NUM,CUST_ID_OLD,newCustID);











	datenow = await (today.getFullYear().toString().substr(-2) + ((today.getMonth() + 1) < 10 ? '0' : '').toString() + (today.getMonth() + 1).toString());

	var dt = new Date().toISOString();

	var dv = dt.substring(0, 10);

	var date_v = dv.substring(0, 4) + dv.substring(5, 7) + dv.substring(8, 10);

	//logger.debug('Date_V:', date_v);

	var tv = dt.substring(11, 19);
	var hp = tv.substring(0, 2);
	var mp = tv.substring(3, 5);
	var sp = tv.substring(6, 8);

	//console.log('Time-7:', hp);

	hp = parseInt(hp) + 7;

	//console.log('Time+7:', hp);

	if (hp > 23) {

		hp = hp - 24;

	}

	if (hp < 9) {

		hp = '0' + hp.toString();
	} else {

		hp = hp.toString();
	}

	var time_v = hp + mp + sp;
	//logger.debug('Time_V:', time_v);

	var dtf = date_v + time_v;

	cards = UpdatePassport.queryCard;
	res.status(200).send({
		"RESP_SYSCDE": "200",
		"RESP_DATETIME": dtf,
		"RESP_CDE": "101",
		"RESP_MSG": "Success UpdatePassport",
		"MCARD_NUM" : MCARD_NUM,
		"CARD_TYPE" :"",
		"CARD_EXPIRY_DATE" :""
	});
	


})
